import java.io.*;
import java.util.HashSet;
import java.util.HashMap;
import java.util.ArrayList;
import java.util.Scanner;
import java.lang.Math;

public class NaiveBayes {
	
	//	This function reads in a file and returns a 
	//	set of all the tokens. It ignores the subject line
	//
	//	If the email had the following content:
	//
	//	Subject: Get rid of your student loans
	//	Hi there ,
	//	If you work for us, we will give you money
	//	to repay your student loans . You will be 
	//	debt free !
	//	FakePerson_22393
	//
	//	This function would return to you
	//	[hi, be, student, for, your, rid, we, get, of, free, if, you, us, give, !, repay, will, loans, work, fakeperson_22393, ,, ., money, there, to, debt]
	public static HashSet<String> tokenSet(File filename) throws IOException {
		HashSet<String> tokens = new HashSet<String>();
		Scanner filescan = new Scanner(filename);
		filescan.next(); //Ignoring "Subject"
		while(filescan.hasNextLine() && filescan.hasNext()) {
			tokens.add(filescan.next());
		}
		filescan.close();
		return tokens;
	}
	/*
	 * Desired test data should be contained in /data/test folder
	 */
	private static final File testFolder = new File("src/data/test");
	private static final File spamFolder = new File("src/data/train/spam");
	private static final File hamFolder = new File("src/data/train/ham");

	private static File[] testPaths;
	private static File[] hamPaths;
	private static File[] spamPaths;
	
	//spam and ham lists
	private static ArrayList<HashSet<String>> spamList;
	private static ArrayList<HashSet<String>> hamList;
	//countedOccurrences
	private static HashMap<String,Double> hamOccur;
	private static HashMap<String,Double> spamOccur;
	
	//calculatedProbabilities
	private static HashMap<String, Double> hamProbs;
	private static HashMap<String, Double> spamProbs;
	private static double pOfHam;
	private static double pOfSpam;
	
	public static void main(String[] args) throws IOException {
		//load up folderlist
		hamPaths = hamFolder.listFiles();
		spamPaths = spamFolder.listFiles();
		
		//get the token sets once so we don't need to keep computing them
		spamList = new ArrayList<HashSet<String>>();
		hamList = new ArrayList<HashSet<String>>();
		for (File f : hamPaths){
			try{
			    hamList.add(tokenSet(f));
			} catch (IOException e){
				System.err.println("Bad File Path");
			}
		}
		
		for (File f : spamPaths){
			try{
			    spamList.add(tokenSet(f));
			} catch (IOException e){
				System.err.println("Bad File Path");
			}
		}
		
		//gather training words
		HashSet<String> trainWordSet = allTrainWords();

		//count
		counts(trainWordSet);

		//build HashMaps of probabilities
		trainProbs(trainWordSet);

		//Calculate probs and output from data/test folder
		testPaths = testFolder.listFiles();
		testAndOutput(trainWordSet);
	}
	
	// make a set of total training words
	private static HashSet<String> allTrainWords(){
		HashSet<String> words = new HashSet<String>();
		for (HashSet<String> h : hamList){
			for (String s : h){
                words.add(s);
			}
		}
		for (HashSet<String> h : spamList){
			for (String s : h){
                words.add(s);
			}
		}
		return words;
	}
	
	//count the occurrences for spam words and ham words
	private static void counts(HashSet<String> words){
		spamOccur = new HashMap<String,Double>();
		hamOccur = new HashMap<String,Double>();
        
		//For every string, count 1 for each, then add 1 for every occurrence
		//Adding into a HashMap, to retain number of occurances for each word
		for (String s : words){
			//spams
	    	spamOccur.put(s, 1.0);
			for (HashSet<String> h : spamList){
				if(h.contains(s)){
					spamOccur.put(s, (spamOccur.get(s))+1.0);
				}
			}
			//hams
	    	hamOccur.put(s, 1.0);
			for (HashSet<String> h : hamList){
				if(h.contains(s)){
					hamOccur.put(s, (hamOccur.get(s))+1.0);
				}
			}
	    }

	}
	//get all the probabilities from 1-4
	private static void trainProbs(HashSet<String> words){
		double curr;
		spamProbs = new HashMap<String,Double>();
		hamProbs = new HashMap<String,Double>();
		double lengthSpam = (double)spamPaths.length;
		double lengthHam = (double)hamPaths.length;
		
		//spams and hams probabilities per word
		for (String s : words){
			//spamOccur s will be occurrences + 1, paths are train emails, add 2 for smoothing
			curr = spamOccur.get(s)/(lengthSpam+2.0);
			spamProbs.put(s, curr);
			curr = hamOccur.get(s)/(lengthHam+2.0);
			hamProbs.put(s,curr);
		}
		pOfHam = lengthHam/(lengthHam+lengthSpam);
		pOfSpam = lengthSpam/(lengthHam+lengthSpam);
		
	}
	//5 test and output the given test files
	private static void testAndOutput(HashSet<String> words){

		//for each test path file
		//dump old sets, get all the words from file, compare against
		//words in test set, only keep words in both, get and add probabilities
		//using Naive Bayes and log rules
		for (File f : testPaths){
			HashSet<String> currFileTokens = new HashSet<String>();
			HashSet<String> finalStrList = new HashSet<String>();
			try{
			    currFileTokens = tokenSet(f);
			} catch (IOException e){
				System.err.println("Bad File Path");
			}
			//prune out words not in our train set
			for (String s : currFileTokens){
                if(words.contains(s)){
                	finalStrList.add(s);
                }
			}
			double pS = Math.log(pOfSpam);
			double pH = Math.log(pOfHam);
			
			//get the probabilities for each word, add to summation 
			for (String s : finalStrList){
				pS += (Math.log(spamProbs.get(s)));
				pH += (Math.log(hamProbs.get(s)));
			}
			
			if(pS>pH){
				System.out.println(f.getName() + " " + "spam");
			} else{
				System.out.println(f.getName() + " " + "ham");
			}
		}
		
	}
	
}

